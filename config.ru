require_relative 'config/users_application'
require_relative 'config/rides_application'
require_relative 'db/database'

# RUN - method provided by RACK
map('/users') do
  run UsersApplication.new
end

map('/rides') do
  run RidesApplication.new
end