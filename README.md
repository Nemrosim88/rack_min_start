#Steps
1. Create Gemfile file
2. $ bundle install
3. Create config.ru file
4. $ bundle exec rackup
#### rackup -> from 'rack' gem
##### it starts ruby build-in web-server (WEBrick)

# Next:
1.Create separate files for db and Application classes.
2. imported them in config.ru file.
3. Created routes for /users and /users/number matched url.

#Next
1. Instead of using routes in Application class, started using 'map' method.
2. Started using "ride = JSON.parse(request.body.read)" for post requests

#Next
For not showing stacktrace of errors:
RACK_ENV=production bundle exec rackup